package com.milysoft.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milysoft.dao.IProductoDAO;
import com.milysoft.model.Producto;
import com.milysoft.service.IProductoService;
@Service
public class ProductoServiceImpl implements IProductoService {
	@Autowired
	private IProductoDAO dao;
	@Override
	public void registrar(Producto pro) {
		// TODO Auto-generated method stub
		dao.save(pro);
		
	}

	@Override
	public void modificar(Producto pro) {
		// TODO Auto-generated method stub
		dao.save(pro);
	}

	@Override
	public void eliminar(int idProducto) {
		// TODO Auto-generated method stub
		dao.delete(idProducto);
	}

	@Override
	public Producto listarId(int idProducto) {
		// TODO Auto-generated method stub
		return dao.findOne(idProducto);
	}

	@Override
	public List<Producto> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
