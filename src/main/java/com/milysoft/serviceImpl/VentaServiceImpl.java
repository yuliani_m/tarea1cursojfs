package com.milysoft.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.milysoft.dao.IVentaDAO;
import com.milysoft.model.Venta;
import com.milysoft.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService{

	@Autowired
	private IVentaDAO dao;
	@Override
	public Venta registrar(Venta venta) {
		venta.getDetalleVenta().forEach(x->x.setVenta(venta));
		
		return dao.save(venta);
	}

	@Override
	public void modificar(Venta venta) {
		// TODO Auto-generated method stub
		dao.save(venta);
	}

	@Override
	public void eliminar(int idVenta) {
		// TODO Auto-generated method stub
		dao.delete(idVenta);
	}

	@Override
	public Venta listarId(int idVenta) {
		// TODO Auto-generated method stub
		return dao.findOne(idVenta);
	}

	@Override
	public List<Venta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
