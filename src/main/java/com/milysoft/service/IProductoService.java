package com.milysoft.service;

import java.util.List;

import com.milysoft.model.Producto;

public interface IProductoService {
	
	void registrar(Producto pro);
	void modificar(Producto pro);
	void eliminar(int idProducto);
	Producto listarId(int idProducto);
	List<Producto> listar();
}
