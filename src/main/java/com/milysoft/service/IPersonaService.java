package com.milysoft.service;

import java.util.List;

import com.milysoft.model.Persona;

public interface IPersonaService {
	
	Persona registrar(Persona per);
	void modificar(Persona per);
	void eliminar(int idPersona);
	Persona listarId(int idPersona);
	List<Persona> listar();

}
